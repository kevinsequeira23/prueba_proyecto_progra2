/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;
    
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 * Esta clase recibe los datos de las habilidades tecnicas
 *
 * @author kevin
 */


public class Logica_Habilidades_Tecnicas2 {
    
    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    private LinkedList<String> datos;
    
    private String HTML;
    private String CSS;
    private String AJAX;
    private String Rubi_on_rails;
    private String Node;
    private String Codelgniter;
    private String Badboy;
    private String Testcomplete;
    private String bugzilla;
    private String Visualestudio;
    private String Eclipse;
    private String netbeans;
    private String Git;
    private String Tortoise;
    private String Bitbucket;

    public Logica_Habilidades_Tecnicas2() {
    }
    
    public void Conexion3() {
        if (getConnection() != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "shemaisrael";
        try {
            Class.forName("org.postgresql.Driver");
            setConnection(DriverManager.getConnection(url, "postgres", password));
            if (getConnection() != null) {
                System.out.println("Connecting to database...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problem when connecting to the database");
        }
    }
    public void insertar() throws IOException{
        Conexion3();
        try {
            
            setS(getConnection().createStatement());
            int z = getS().executeUpdate("INSERT INTO habilidades_tecnicas_2(html,css,ajax,ruby_on_rails,node_js,codeigniter,visualstudio_net,eclipse,netbeans,git,tortoise,bitbucket,badboy,testcomplete,bugzilla,id_user) VALUES ('" + getHTML() + "', '" + getCSS() + "', '" + getAJAX() + "', '" + getRubi_on_rails() +"', '"+getNode()+"', '"+getCodelgniter()+"', '" + getVisualestudio() +"', '" +getEclipse()+ "', '" +getNetbeans()+"', '"+getGit()+"', '"+getTortoise()+"', '"+getBitbucket()+"', '"+getBadboy()+"', '"+getTestcomplete()+"', '"+getBugzilla()+"', '" +leer()+"')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (SQLException e) {
            System.out.println("Error de conexión");
        }
    }  
    
    
    public int leer() throws IOException {
        int recibir=0;
        BufferedReader bf = new BufferedReader(new FileReader("buzon/Temporal.txt"));
        String sCadena;
        while ((sCadena = bf.readLine())!=null) {
//            System.out.println(sCadena);
        recibir = Integer.valueOf(sCadena);
}
        return recibir;
        
    }
    
    
    
    
    
    

    /**
     * @return the HTML
     */
    public String getHTML() {
        return HTML;
    }

    /**
     * @param HTML the HTML to set
     */
    public void setHTML(String HTML) {
        this.HTML = HTML;
    }

    /**
     * @return the CSS
     */
    public String getCSS() {
        return CSS;
    }

    /**
     * @param CSS the CSS to set
     */
    public void setCSS(String CSS) {
        this.CSS = CSS;
    }

    /**
     * @return the AJAX
     */
    public String getAJAX() {
        return AJAX;
    }

    /**
     * @param AJAX the AJAX to set
     */
    public void setAJAX(String AJAX) {
        this.AJAX = AJAX;
    }

    /**
     * @return the Rubi_on_rails
     */
    public String getRubi_on_rails() {
        return Rubi_on_rails;
    }

    /**
     * @param Rubi_on_rails the Rubi_on_rails to set
     */
    public void setRubi_on_rails(String Rubi_on_rails) {
        this.Rubi_on_rails = Rubi_on_rails;
    }

    /**
     * @return the Node
     */
    public String getNode() {
        return Node;
    }

    /**
     * @param Node the Node to set
     */
    public void setNode(String Node) {
        this.Node = Node;
    }

    /**
     * @return the Codelgniter
     */
    public String getCodelgniter() {
        return Codelgniter;
    }

    /**
     * @param Codelgniter the Codelgniter to set
     */
    public void setCodelgniter(String Codelgniter) {
        this.Codelgniter = Codelgniter;
    }

    /**
     * @return the Badboy
     */
    public String getBadboy() {
        return Badboy;
    }

    /**
     * @param Badboy the Badboy to set
     */
    public void setBadboy(String Badboy) {
        this.Badboy = Badboy;
    }

    /**
     * @return the Testcomplete
     */
    public String getTestcomplete() {
        return Testcomplete;
    }

    /**
     * @param Testcomplete the Testcomplete to set
     */
    public void setTestcomplete(String Testcomplete) {
        this.Testcomplete = Testcomplete;
    }

    /**
     * @return the bugzilla
     */
    public String getBugzilla() {
        return bugzilla;
    }

    /**
     * @param bugzilla the bugzilla to set
     */
    public void setBugzilla(String bugzilla) {
        this.bugzilla = bugzilla;
    }

    /**
     * @return the Visualestudio
     */
    public String getVisualestudio() {
        return Visualestudio;
    }

    /**
     * @param Visualestudio the Visualestudio to set
     */
    public void setVisualestudio(String Visualestudio) {
        this.Visualestudio = Visualestudio;
    }

    /**
     * @return the Eclipse
     */
    public String getEclipse() {
        return Eclipse;
    }

    /**
     * @param Eclipse the Eclipse to set
     */
    public void setEclipse(String Eclipse) {
        this.Eclipse = Eclipse;
    }

    /**
     * @return the netbeans
     */
    public String getNetbeans() {
        return netbeans;
    }

    /**
     * @param netbeans the netbeans to set
     */
    public void setNetbeans(String netbeans) {
        this.netbeans = netbeans;
    }

    /**
     * @return the Git
     */
    public String getGit() {
        return Git;
    }

    /**
     * @param Git the Git to set
     */
    public void setGit(String Git) {
        this.Git = Git;
    }

    /**
     * @return the Tortoise
     */
    public String getTortoise() {
        return Tortoise;
    }

    /**
     * @param Tortoise the Tortoise to set
     */
    public void setTortoise(String Tortoise) {
        this.Tortoise = Tortoise;
    }

    /**
     * @return the Bitbucket
     */
    public String getBitbucket() {
        return Bitbucket;
    }

    /**
     * @param Bitbucket the Bitbucket to set
     */
    public void setBitbucket(String Bitbucket) {
        this.Bitbucket = Bitbucket;
    }
    
    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the s
     */
    public Statement getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(Statement s) {
        this.s = s;
    }
        public void tecnicas1(String cedula) {
        datos = new LinkedList<>();
        System.out.println(cedula);
        Conexion3();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT html, css,ajax,ruby_on_rails,node_js,codeigniter,visualstudio_net,eclipse,netbeans,git,tortoise,bitbucket,badboy,testcomplete,bugzilla FROM habilidades_tecnicas_2 WHERE id_user = '" + Integer.parseInt(cedula) + "'");
            while (rs.next()) {
                datos.add(rs.getString("html")
                        + "," + rs.getString("css")
                        + "," + rs.getString("ajax")
                        + "," + rs.getString("ruby_on_rails")
                        + "," + rs.getString("node_js")
                        + "," + rs.getString("codeigniter")
                        + "," + rs.getString("visualstudio_net")
                        + "," + rs.getString("eclipse")
                        + "," + rs.getString("netbeans")
                        + "," + rs.getString("git")
                        + "," + rs.getString("tortoise")
                        + "," + rs.getString("bitbucket")
                        + "," + rs.getString("badboy")
                        + "," + rs.getString("testcomplete")
                        + "," + rs.getString("bugzilla")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> retornaDatos() {
        return datos;
    }
    public void modificar(String id){
         Conexion3();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE habilidades_tecnicas_2 SET html = '" + getHTML() + "', css = '" + getCSS() + "', ajax = '" +getAJAX() +"', ruby_on_rails = '" +getRubi_on_rails() +"', node_js = '" +getNode() +"', codeigniter = '" +getCodelgniter() +"', visualstudio_net = '" +getVisualestudio() +"', eclipse = '" +getEclipse() +"', netbeans = '" +getNetbeans() +"', git = '" +getGit() +"', tortoise = '" +getTortoise() +"', bitbucket = '" +getBitbucket() +"', badboy = '" +getBadboy() +"', testcomplete = '" +getTestcomplete() +"', bugzilla = '" +getBugzilla() + "' WHERE id_user = '" + Integer.parseInt(id) + "'");
            if (z == 1) {
                System.out.println("Se módificó el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
     }

}
