/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaGuardar;

import java.awt.BorderLayout;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxmaps.swing.MapView;

import com.teamdev.jxmaps.*;

/**
 *
 * @author kevin
 */
public class Mapa extends MapView {

    private Map map;
    /**
     * this is a map
     * @param name
     * @param latitud
     * @param longitud 
     */
    public Mapa(String name, double latitud, double longitud) {

        setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status) {
                if (status == MapStatus.MAP_STATUS_OK) {
                    map = getMap();
                    MapOptions mapOptions = new MapOptions();
                    MapTypeControlOptions controlOptions = new MapTypeControlOptions();
                    mapOptions.setMapTypeControlOptions(controlOptions);

                    map.setOptions(mapOptions);
                    map.setCenter(new LatLng(latitud, longitud));
                    map.setZoom(11.0);

//                    // marcador
//                    Marker mark = new   Marker(map);
//                    mark.setPosition(map.getCenter());
//                    Circle circle = new Circle(map);
//                    circle.setCenter(map.getCenter());
//                    circle.setRadius(400);
//                    
//                    CircleOptions co = new CircleOptions();
//                    co.setFillColor("#FF0000");
//                    co.setFillOpacity(0.35);
//                    circle.setOptions(co);
                }
            }
        });
        JFrame frame = new JFrame(name);
        frame.add(this, BorderLayout.CENTER);
        frame.setLocationRelativeTo(null);
        frame.setSize(600, 600);
        frame.setVisible(true);

    }

}
