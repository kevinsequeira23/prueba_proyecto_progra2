/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 * Esta clase recibe todos los datos de las habilidades tecnicas
 *
 * @author kevin
 */
public class Logica_Habilidades_tecnicas {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;

    private String microsoft_proyect;
    private String jira;
    private String confluence;
    private String scrum;
    private String extreme_programing;
    private String kanban;
    private String c;
    private String java;
    private String python;
    private String mySQL;
    private String oracle;
    private String mongoBD;
    private int cedula2;
    private LinkedList<String> datos;

    public Logica_Habilidades_tecnicas() {
    }

    public void Conexion2() {
        if (getConnection() != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "shemaisrael";
        try {
            Class.forName("org.postgresql.Driver");
            setConnection(DriverManager.getConnection(url, "postgres", password));
            if (getConnection() != null) {
                System.out.println("Connecting to database...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problem when connecting to the database");
        }
    }

    public void insertar() throws IOException {
        Conexion2();
        try {

            setS(getConnection().createStatement());
            int z = getS().executeUpdate("INSERT INTO habilidades_tecnicas_1(microsoft_project,jira,confluence,scrum,extreme_programming,kanban,c,java,python,mysql,oracle,mongodb,id_usuario) VALUES ('" + getMicrosoft_proyect() + "', '" + getJira() + "', '" + getConfluence() + "', '" + getScrum() + "', '" + getExtreme_programing() + "', '" + getKanban() + "', '" + getC() + "', '" + getJava() + "', '" + getPython() + "', '" + getMySQL() + "', '" + getOracle() + "', '" + getMongoBD() + "', '" + leer() + "')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (SQLException e) {
            System.out.println("Error de conexión");
        }
    }

    public int leer() throws IOException {
        int recibir = 0;
        BufferedReader bf = new BufferedReader(new FileReader("buzon/Temporal.txt"));
        String sCadena;
        while ((sCadena = bf.readLine()) != null) {
//            System.out.println(sCadena);
            recibir = Integer.valueOf(sCadena);
        }
        return recibir;

    }

    public void crear(String cedula) throws IOException, IOException {
        String ruta = "buzon/Temporal.txt";
        File archivo = new File(ruta);
        BufferedWriter bw;
        if (archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            eliminarFichero(archivo);
            bw.write(cedula);
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(cedula);
        }

        bw.close();

    }

    public static void eliminarFichero(File archivo) {

        if (!archivo.exists()) {
            System.out.println("El archivo data no existe.");
        } else {
            archivo.delete();
            System.out.println("El archivo data fue eliminado.");
        }

    }

    /**
     * @return the microsoft_proyect
     */
    public String getMicrosoft_proyect() {
        return microsoft_proyect;
    }

    /**
     * @param microsoft_proyect the microsoft_proyect to set
     */
    public void setMicrosoft_proyect(String microsoft_proyect) {
        this.microsoft_proyect = microsoft_proyect;
    }

    /**
     * @return the jira
     */
    public String getJira() {
        return jira;
    }

    /**
     * @param jira the jira to set
     */
    public void setJira(String jira) {
        this.jira = jira;
    }

    /**
     * @return the confluence
     */
    public String getConfluence() {
        return confluence;
    }

    /**
     * @param confluence the confluence to set
     */
    public void setConfluence(String confluence) {
        this.confluence = confluence;
    }

    /**
     * @return the scrum
     */
    public String getScrum() {
        return scrum;
    }

    /**
     * @param scrum the scrum to set
     */
    public void setScrum(String scrum) {
        this.scrum = scrum;
    }

    /**
     * @return the extreme_programing
     */
    public String getExtreme_programing() {
        return extreme_programing;
    }

    /**
     * @param extreme_programing the extreme_programing to set
     */
    public void setExtreme_programing(String extreme_programing) {
        this.extreme_programing = extreme_programing;
    }

    /**
     * @return the kanban
     */
    public String getKanban() {
        return kanban;
    }

    /**
     * @param kanban the kanban to set
     */
    public void setKanban(String kanban) {
        this.kanban = kanban;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(String c) {
        this.c = c;
    }

    /**
     * @return the java
     */
    public String getJava() {
        return java;
    }

    /**
     * @param java the java to set
     */
    public void setJava(String java) {
        this.java = java;
    }

    /**
     * @return the python
     */
    public String getPython() {
        return python;
    }

    /**
     * @param python the python to set
     */
    public void setPython(String python) {
        this.python = python;
    }

    /**
     * @return the mySQL
     */
    public String getMySQL() {
        return mySQL;
    }

    /**
     * @param mySQL the mySQL to set
     */
    public void setMySQL(String mySQL) {
        this.mySQL = mySQL;
    }

    /**
     * @return the oracle
     */
    public String getOracle() {
        return oracle;
    }

    /**
     * @param oracle the oracle to set
     */
    public void setOracle(String oracle) {
        this.oracle = oracle;
    }

    /**
     * @return the mongoBD
     */
    public String getMongoBD() {
        return mongoBD;
    }

    /**
     * @param mongoBD the mongoBD to set
     */
    public void setMongoBD(String mongoBD) {
        this.mongoBD = mongoBD;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the s
     */
    public Statement getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(Statement s) {
        this.s = s;
    }

    public void tecnicas1(String cedula) {
        datos = new LinkedList<>();
        System.out.println(cedula);
        Conexion2();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT microsoft_project, jira,confluence,scrum,extreme_programming,kanban,c,java,python,mysql,oracle,mongodb FROM habilidades_tecnicas_1 WHERE id_usuario = '" + Integer.parseInt(cedula) + "'");
            while (rs.next()) {
                datos.add(rs.getString("microsoft_project")
                        + "," + rs.getString("jira")
                        + "," + rs.getString("confluence")
                        + "," + rs.getString("scrum")
                        + "," + rs.getString("extreme_programming")
                        + "," + rs.getString("kanban")
                        + "," + rs.getString("c")
                        + "," + rs.getString("java")
                        + "," + rs.getString("python")
                        + "," + rs.getString("mysql")
                        + "," + rs.getString("oracle")
                        + "," + rs.getString("mongodb")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> retornaDatos() {
        return datos;
    }
     public void modificar(String id){
         Conexion2();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE habilidades_tecnicas_1 SET microsoft_project = '" + getMicrosoft_proyect() + "', jira = '" + getJira() + "', confluence = '" +getConfluence() +"', scrum = '" +getScrum() +"', extreme_programming = '" +getExtreme_programing() +"', kanban = '" +getKanban() +"', c = '" +getC() +"', java = '" +getJava() +"', python = '" +getPython() +"', mysql = '" +getMySQL() +"', oracle = '" +getOracle() +"', mongodb = '" +getMongoBD() + "' WHERE id_usuario = '" + id + "'");
            if (z == 1) {
                System.out.println("Se módificó el registro de manera exitosa");
            } else {
                System.out.println("Error al modificar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
     }
}
