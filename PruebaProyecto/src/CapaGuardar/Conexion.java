
package CapaGuardar;

import CapaInterfaz2.HabilidadesTecnicas;
import CapaInterfaz2.Seccion_administrador;
import Datos2.Grafica_Habilidades;
import Datos2.Graficas_Mongo;
import Datos2.Registro_Usuarios;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;


/**
 *
 * @author MONGE
 */
public class Conexion {
    
    private Connection connection = null;
    private ResultSet rs = null;
    private ResultSet rs2 = null;
    private ResultSet rs3 = null;
    private ResultSet rs4 = null;
    private Statement s = null;
    private Registro_Usuarios re = new Registro_Usuarios();
    private static String cedulaStatico;

    
    private String nombre;
    private int cedula;
    private String apellido;
    private String apellido2;
    private String contraseña;
    private int edad;
    private Date fecha;
    private String provinciaU;
    private String cantonY;
    private String distritoU;
    private boolean estado;
    
    private int puntage;
    private int id_user;
    private String nivel;
    
    public Conexion() {
    }
     /**
      * this is a conection whith postgresql
      */
    public void Conexion() {
        if (getConnection() != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "shemaisrael";
        try {
            Class.forName("org.postgresql.Driver");
            setConnection(DriverManager.getConnection(url, "postgres", password));
            if (getConnection() != null) {
                System.out.println("Connecting to database...");
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problem when connecting to the database");
        }
    }
    /**
     * its method insert dates in the database
     */
    public void insertar(){
        Conexion();
        try {
            String estate = "";
            if(estado){
                estate = "Activo";
            }
            setS(getConnection().createStatement());
            int z = getS().executeUpdate("INSERT INTO Usuarios(cedula,nombre,primer_apellido,segundo_apellido,fecha_de_nasimiento,edad,contraseña,provincia_usuario,canton_usuario,distrito_usuario,estado,tipo) VALUES ('" + getCedula() + "', '" + getNombre() + "', '" + getApellido() + "', '" + getApellido2() +"', '"+getFecha()+"', '"+getEdad()+"', '" + getContraseña() +"', '" +getProvinciaU()+ "', '" +getCantonY()+"', '" +getDistritoU()+"', '" +estate+"', '" +"Usuario"+"')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (SQLException e) {
            System.out.println("Error de conexión");
        }
    } 
    /**
     * Validation of users
     * @param cedula2
     * @param contra 
     */
    public void validar_Usuario_y_Contrasena_y_estado(String cedula2, String contra){
        String cap="";
        String cap2="";
        String cap3="";
        String cap4="";
        String cont="";
        Conexion();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT cedula FROM Usuarios WHERE cedula = '" + cedula2 + "'");
            
            while (rs.next()) {
                cap=rs.getString("cedula");               
            }   
            rs2 =s.executeQuery("SELECT contraseña FROM Usuarios WHERE cedula = '" + cap + "'");
            while (rs2.next()) {
                cap2 = rs2.getString("contraseña");               
            } 
            
             rs3 =s.executeQuery("SELECT estado FROM Usuarios WHERE cedula = '" + cap + "'");
             while (rs3.next()) {
                cap3 = rs3.getString("estado");               
             } 

            if(cedula2.equals("123456789") && contra.equals("123")){
                Seccion_administrador admin = new Seccion_administrador(null, true);
                admin.pack();
                admin.setVisible(true);
            }
             
            else if(cap.equals(cedula2) && cap2.equals(contra)&& cap3.equals("Activo")){
                
                setCedulaStatico(cedula2);
                HabilidadesTecnicas habilidades = new HabilidadesTecnicas(null,true);
                habilidades.pack();
                habilidades.setVisible(true);
                
                
            }else if(!cap.equals(cedula2) ||!cap2.equals(contra)){
                JOptionPane.showMessageDialog(null,"Usuario o contraseña incorrecto");
                 
            }else if(!cap3.equals("Activo")){
                JOptionPane.showMessageDialog(null,"Tu estado está en inactivo");
            }
        } catch (SQLException e) {
            System.out.println("Error de conexión");
        }
    }
    /**
     * validation of users repeat
     * @param cedula_repetida
     * @return 
     */
    public boolean validar_Usuarios_Repetidos(String cedula_repetida){
        Conexion();
        String cedulas_que_coinciden = "";
        
        try {
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT cedula from usuarios where cedula ='" + cedula_repetida + "'");
            
            while (rs.next()) {
                cedulas_que_coinciden = rs.getString("cedula");              
            }
            if(cedulas_que_coinciden.equals(cedula_repetida)){
                return true;
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
    /**
     * list of country
     * @param numero_Provincia
     * @return 
     */
    public ArrayList cantones(int numero_Provincia){
        ArrayList <String> cantones = new ArrayList<>();
        Conexion();
        String nombres = "";
        try {

            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT nombre from cantones where codigo_provincia ='" + numero_Provincia + "'");
            
            while (rs.next()) {
                nombres = rs.getString("nombre");
                cantones.add(nombres);
            }
        } catch (Exception e) {
            System.out.println("No es posible hacer la búsqueda de cantones");
        }

        return cantones;
        
    }
    /**
     * list of country
     * @param numero_Canton
     * @return 
     */
     public ArrayList distritos(int numero_Canton){
        ArrayList <String> cantones = new ArrayList<>();
        Conexion();
        String nombres = "";
        try {

            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT nombre_distrito from distritos where codigo_canton ='" + numero_Canton + "'");
            while (rs.next()) {
                nombres = rs.getString("nombre_distrito");
                cantones.add(nombres);
            }
        } catch (Exception e) {
            System.out.println("No es posible hacer la búsqueda de distritos");
        }
        return cantones;     
    }
     
     
     /**
      * 
      * @param ced
      * @return 
      */
     public String verif_estado(String ced){
         
        Conexion();
        String estado = "";
        try {

            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT estado from usuarios where cedula ='" + ced + "'");
            
            while (rs.next()) {
                estado = rs.getString("estado");
                
                
                
            }
        } catch (Exception e) {
            System.out.println("Error al conectar con la base");
        }
         return estado;
     }
     /**
      * date new for estate
     * @param valor_nuevo_para_el_estado
      * @param cedu
      * @return 
      */
     public String cambiar_estado(String valor_nuevo_para_el_estado, String cedu){
         Conexion();
         String res = "";
        try {
          
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE usuarios SET estado = '" + valor_nuevo_para_el_estado + "' WHERE cedula = '" + cedu + "'");
            if (z == 1) {
               res = "Se módificó el registro de manera exitosa";
            } else {
                res = "Error al modificar el registro";
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
        return res;
     }
     /**
      * earch users
      * @param ced
      * @return 
      */
     public ArrayList buscar_usuario_por_cedula(String ced){
         double latitud=0;
         double longitud=0;
         String distrito="";
         ArrayList usuarios = new ArrayList();
         Conexion();
         String res = "";
         String res2 = "";
         try {
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT nombre,primer_apellido,segundo_apellido,fecha_de_nasimiento,edad,distrito_usuario from usuarios where cedula ='" + ced + "'");
             while (rs.next()) {
                usuarios.add("NOMRE: ");
                res = rs.getString("nombre");
                usuarios.add(res);
                usuarios.add("PRIMER APELLIDO: ");
                res = rs.getString("primer_apellido");
                usuarios.add(res);
                usuarios.add("SEGUNDO APELLIDO: ");
                res = rs.getString("segundo_apellido");
                usuarios.add(res);
                usuarios.add("FECHA DE NACIMIENTO: ");
                res = rs.getString("fecha_de_nasimiento");
                usuarios.add(res);
                usuarios.add("EDAD: ");
                res = rs.getString("edad");
                usuarios.add(res);
   
            }
            
             
         } catch (Exception e) {
         }
         
         try {
             s = getConnection().createStatement();
            rs = s.executeQuery("SELECT nivel_de_liderazgo from habilidades_blandas where cedula ='" + ced + "'");
            while(rs.next()){
                usuarios.add("NIVEL DE LIDERAZGO ");
                res2 = rs.getString("nivel_de_liderazgo");
                usuarios.add(res2);
            }
         } catch (Exception e) {
         }
         
         
         System.out.println(usuarios);
         return usuarios;
     }

     /**
      *  
        shows advanced users in a branch
      */
     public ArrayList mostrarAvanzados_python(String ced){
         ArrayList usuarios = new ArrayList();
         Conexion();
         String res = "";
         
         try {
             s = getConnection().createStatement();
            rs = s.executeQuery("SELECT cedula,nombre,primer_apellido,segundo_apellido from usuarios where cedula ='" + ced + "'");
             
         } catch (Exception e) {
         }
         

         try {
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT cedula,nombre,primer_apellido,segundo_apellido from usuarios where cedula ='" + ced + "'");
 
             while (rs.next()) {
                usuarios.add("CEDULA: ");
                res = rs.getString("cedula");
                usuarios.add(res);
                usuarios.add("NOMBRE: ");
                res = rs.getString("nombre");
                usuarios.add(res);
                usuarios.add("PRIMER APELLIDO: ");
                res = rs.getString("primer_apellido");
                usuarios.add(res);
                usuarios.add("SEGUNDO APELLIDO: ");
                res = rs.getString("segundo_apellido");
                usuarios.add(res);
   
            }
         } catch (Exception e) {
         }
         
         
         
         return usuarios;
     }
     
     
     
//      public ArrayList todos_los_usuarios(){
//        
//         Conexion();
//         ArrayList todos_los_usuarios = new ArrayList();
//         String n = "";
//         String n1 = "";
//         String n2 = "";
//         String n3 = "";
//         String n4 = "";
//         String n5 = "";
//         String n6 = "";
//         String divi = "--------------------------------------------------------";
//          try {
//
//            s = getConnection().createStatement();
//            rs = s.executeQuery("SELECT cedula from usuarios where tipo ='" + "Usuario"+ "'");
//            
//            while (rs.next()) {
//                
//                
//                todos_los_usuarios.add("CEDULA: ");
//                n = rs.getString("cedula");
//                todos_los_usuarios.add(n);
////                todos_los_usuarios.add("NOMBRE: ");
////                n1 = rs.getString("nombre");
////                todos_los_usuarios.add(n1);
////                todos_los_usuarios.add("PRIMER APELLIDO: ");
////                n2 = rs.getString("primer_apellido");
////                todos_los_usuarios.add(n2);
////                todos_los_usuarios.add("SEGUNDO APELLIDO: ");
////                n3 = rs.getString("segundo_apellido");
////                todos_los_usuarios.add(n3);
////                todos_los_usuarios.add("EDAD: ");
////                n4 = rs.getString("edad");
////                todos_los_usuarios.add(n4);
////                todos_los_usuarios.add("DISTRITO: ");
////                n5 = rs.getString("distrito_usuario");
////                todos_los_usuarios.add(n5);
////                todos_los_usuarios.add("ESTADO: ");
////                n6 = rs.getString("estado");
////                todos_los_usuarios.add(n6);
//            }
//        } catch (Exception e) {
//            System.out.println("Error al conectar con la base");
//        }
//         
//          
//         return todos_los_usuarios;
//     }
     
     
     public  ArrayList  usu(){
         Conexion();
         ArrayList todos_los_usuarios = new ArrayList();
         String c = "";
         String c1 = "";
         String c2 = "";
         String c3 = "";
         String c4 = "";
         String c5 = "";
         String c6 = "";
         
         
         try {
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT cedula,nombre,primer_apellido,segundo_apellido,edad,distrito_usuario,estado from usuarios where tipo ='" + "Usuario" + "'");
            while(rs.next()){
                todos_los_usuarios.add("CEDULA:");
                c = rs.getString("cedula");
                todos_los_usuarios.add(c);
                todos_los_usuarios.add("NOMBRE:");
                c1 = rs.getString("nombre");
                todos_los_usuarios.add(c1);
                todos_los_usuarios.add("PRIMER APELLIDO:");
                c2 = rs.getString("primer_apellido");
                todos_los_usuarios.add(c2);
                todos_los_usuarios.add("SEGUNDO APELLIDO:");
                c3 = rs.getString("segundo_apellido");
                todos_los_usuarios.add(c3);
                todos_los_usuarios.add("EDAD:");
                c4 = rs.getString("edad");              
                todos_los_usuarios.add(c4);
                todos_los_usuarios.add("DISTRITO:");
                c5 = rs.getString("distrito_usuario");
                todos_los_usuarios.add(c5);
                todos_los_usuarios.add("ESTADO:");
                c6 = rs.getString("estado");
                todos_los_usuarios.add(c6);
                todos_los_usuarios.add("----------------------------------------");
            }
         } catch (Exception e) {
         }
         
         return todos_los_usuarios;
     }
     public void insertar6() throws IOException{
        Conexion();
        try {
            
            setS(getConnection().createStatement());
            int z = getS().executeUpdate("INSERT INTO habilidades_blandas(puntage_obtenido,nivel_de_liderazgo,id_user) VALUES ('" + getPuntage() + "', '"+getNivel() + "', '" +leer()+"')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (SQLException e) {
            System.out.println("Error de conexión");
        }
    }  
    
    
    public int leer() throws IOException {
        int recibir=0;
        BufferedReader bf = new BufferedReader(new FileReader("buzon/Temporal.txt"));
        String sCadena;
        while ((sCadena = bf.readLine())!=null) {
//            System.out.println(sCadena);
        recibir = Integer.valueOf(sCadena);
}
        return recibir;
        
    }
    
    public void contar(){
         Conexion();
         int basico_mongo_DB = 0;
         int intermedio_mongo_DB = 0;
         int avanzado_mongo_DB = 0;
         
         try {
             
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_usuario from habilidades_tecnicas_1 where mongodb ='" + "basico"+ "'");
            while(rs.next()){
                basico_mongo_DB++;
            }
             
         } catch (Exception e) {
            
         }
         try {
             
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_usuario from habilidades_tecnicas_1 where mongodb ='" + "intermedio"+ "'");
            while(rs.next()){
                intermedio_mongo_DB++;
            }
             
         } catch (Exception e) {
            
         }
         try {
             
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_usuario from habilidades_tecnicas_1 where mongodb ='" + "avanzado"+ "'");
            while(rs.next()){
                avanzado_mongo_DB++;
            }
             
         } catch (Exception e) {
            
         }
         System.out.println("ba"+ basico_mongo_DB);
         System.out.println("in"+ intermedio_mongo_DB);
         System.out.println("av"+ avanzado_mongo_DB);
         Graficas_Mongo mongo = new Graficas_Mongo();
         mongo.graficarPastelMongo(basico_mongo_DB, intermedio_mongo_DB, avanzado_mongo_DB);
         
         
     }
    
    public void contar_liderazgo(){
        Conexion();
        int intermedio = 0;
        int avanzado = 0;
         try {
             
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_user from habilidades_blandas where nivel_de_liderazgo ='" + "intermedio"+ "'");
            while(rs.next()){
                intermedio++;
            }
             
         } catch (Exception e) {
            
         }
         try {
             
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_user from habilidades_blandas where nivel_de_liderazgo ='" + "avanzado"+ "'");
            while(rs.next()){
                avanzado++;
            }
             
         } catch (Exception e) {
            
         }
        
        Grafica_Habilidades h = new Grafica_Habilidades();
        System.out.println(intermedio);
        System.out.println(avanzado);
        h.graficarPastelLider(intermedio, avanzado);
    }
    
     public void consultar_latitud_y_longitud(int ced){
        Conexion();
        String lat = "";
        String lon = "";
        String canto = "";
        try {
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT canton_usuario from usuarios where cedula ='" +ced+ "'");
            while(rs.next()){
                canto = rs.getString("canton_usuario");
                System.out.println(canto);
                             
            }
        } catch (Exception e) {
            System.out.println("No se puedo realizar la búsqueda");
        }
        
        try {
            System.out.println(canto);
            s = getConnection().createStatement();
            rs = s.executeQuery("SELECT latitud,longitud from cantones where nombre='" +canto+ "'");
            while(rs.next()){
                lat = rs.getString("latitud");
                lon = rs.getString("longitud");
                             
            }
        } catch (Exception e) {
            System.out.println("No se puedo realizar la búsqueda");
        }
        double la = Double.parseDouble(lat);
        double lo = Double.parseDouble(lon);
        System.out.println(la);
        System.out.println(lo);
        Mapa pintar_mapa = new Mapa(canto, la, lo);
        
        
        
    }
     
     public ArrayList avanzados_en_python(){
         Conexion();
         String info = "";
         String info2 = "";
         ArrayList py = new ArrayList();
         try {
             s = getConnection().createStatement();
            rs = s.executeQuery("SELECT id_usuario from habilidades_tecnicas_1 where python ='" +"avanzado"+ "'");
            while(rs.next()){
                info = rs.getString("id_usuario");
                py.add(info);
            }
            
         } catch (Exception e) {
         }
         System.out.println(py);
         return py;
     }
        public ArrayList avanzados_en_python2(){
         Conexion();
         String info = "";
         String info2 = "";
         String cedul = "";
         ArrayList lista_usuarios = new ArrayList();
         ArrayList lista_usuarios2 = new ArrayList();
         
         lista_usuarios = avanzados_en_python();
            try {
                for (int i = 0; i < lista_usuarios.size(); i++) {
                     s = getConnection().createStatement();
                    rs = s.executeQuery("SELECT cedula,nombre,primer_apellido from usuarios where cedula ='" +lista_usuarios.get(i)+ "'");
                    while(rs.next()){
                    lista_usuarios2.add("---------------------------------------------------------------------");
                    lista_usuarios2.add("CEDULA");
                    info = rs.getString("cedula");
                    lista_usuarios2.add(info);
                    lista_usuarios2.add("NOMBRE");
                    info = rs.getString("nombre");
                    lista_usuarios2.add(info);
                    lista_usuarios2.add("APELLIDO");
                    info = rs.getString("primer_apellido");
                    lista_usuarios2.add(info);
                }
                 }
               
                
              
            } catch (Exception e) {
            }
            
         
         
            
         return lista_usuarios2;
     }
                    
     
     
     
     
     
     
     
     

     
     
     
     
     
     
     
     
     
     
     
     
     
     
    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the s
     */
    public Statement getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(Statement s) {
        this.s = s;
    }

    /**
     * @return the re
     */
    public Registro_Usuarios getRe() {
        return re;
    }

    /**
     * @param re the re to set
     */
    public void setRe(Registro_Usuarios re) {
        this.re = re;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cedula
     */
    public int getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * @param apellido2 the apellido2 to set
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * @return the contraseña
     */
    public String getContraseña() {
        return contraseña;
    }

    /**
     * @param contraseña the contraseña to set
     */
    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getProvinciaU() {
        return provinciaU;
    }

    public void setProvinciaU(String provinciaU) {
        this.provinciaU = provinciaU;
    }

    public String getCantonY() {
        return cantonY;
    }

    public void setCantonY(String cantonY) {
        this.cantonY = cantonY;
    }

    public String getDistritoU() {
        return distritoU;
    }

    public void setDistritoU(String distritoU) {
        this.distritoU = distritoU;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getPuntage() {
        return puntage;
    }

    public void setPuntage(int puntage) {
        this.puntage = puntage;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    
    public static String getCedulaStatico() {
        return cedulaStatico;
    }

    public static void setCedulaStatico(String cedulaStatico) {
        Conexion.cedulaStatico = cedulaStatico;
    }
    

    }

