/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;

import CapaInterfaz2.BuscarUsuarioPorCedula;
import CapaInterfaz2.Cambiar_estado;
import CapaInterfaz2.HabilidadesBlandas;
import CapaInterfaz2.HabilidadesTecnicas;
import CapaInterfaz2.HabilidadesTecnicas2;
import CapaInterfaz2.Login;
import CapaInterfaz2.Mostrar_todos_los_usuarios;
import CapaInterfaz2.Repor_tes;
import CapaInterfaz2.Seccion_administrador;
import CapaInterfaz2.SigIn;
import static com.sun.javafx.tk.Toolkit.getToolkit;
import javax.swing.JOptionPane;

/**
 *
 * @author Samir Portillo
 */
public class Llamar_todas_las_ventana {
    
    
    
    
    public void llamarLogIn(){
        Login x = new Login(null, true);
        x.pack();
        x.setVisible(true);
    }
    
    public void llamarSigIn(){
        SigIn x = new SigIn(null, true);
        x.pack();
        x.setVisible(true);
    }
    
     public void llamarSeccionAdmin(){
        Seccion_administrador x = new Seccion_administrador(null, true);
        x.pack();
        x.setVisible(true);
    }
    
     public void llamarSeccionAdmin_Lista_usuarios_registrados(){
        Mostrar_todos_los_usuarios x = new Mostrar_todos_los_usuarios(null, true);
        x.pack();
        x.setVisible(true);
    }
      public void llamarSeccionAdmin_cambiar_estado(){
          Cambiar_estado x = new Cambiar_estado(null, true);
        x.pack();
        x.setVisible(true);
    }
      
       public void llamarSeccionAdmin_buscar_por_cedula(){
        BuscarUsuarioPorCedula x = new BuscarUsuarioPorCedula(null, true);
        x.pack();
        x.setVisible(true);
    }
      
       public void llamarSeccionAdmin_reportes(){
        Repor_tes x = new Repor_tes();
        x.pack();
        x.setVisible(true);
    }
    
    
      public void habilidades_blandas(){
        HabilidadesBlandas x = new HabilidadesBlandas (null, true);
        x.pack();
        x.setVisible(true);
    }
      public void habilidades_tecnicas(){
          HabilidadesTecnicas x = new HabilidadesTecnicas(null, true);
        x.pack();
        x.setVisible(true);
    }
      
       public void habilidades_tecnicas2(){
        HabilidadesTecnicas2 x = new HabilidadesTecnicas2(null, true);
        x.pack();
        x.setVisible(true);
    }
       
    public void cambiarEstado(){
        Cambiar_estado x = new Cambiar_estado(null, true);
        x.pack();
        x.setVisible(true);
    }
    
    
    
    
     public void soloLetrasKeyTyped(java.awt.event.KeyEvent evt) {
        char a = evt.getKeyChar();
        if (Character.isDigit(a)) {
            
            evt.consume();
            JOptionPane.showMessageDialog(null, "No se permiten numeros", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void soloNumerosKeyTyped(java.awt.event.KeyEvent evt) {
        char a = evt.getKeyChar();
        if (Character.isLetter(a)) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "No se permiten letras", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    
    public boolean restringirCamposKeyTyped(java.awt.event.KeyEvent evt, int cantidadCaracteres){
      int numeroCaracteres = 8;
      
        if(numeroCaracteres == cantidadCaracteres){
            evt.consume();
            JOptionPane.showMessageDialog(null,"Digite su cédula con todos sus números", "Error", JOptionPane.ERROR_MESSAGE);
            return true;

        } 
        return false;
    }
    
    
    
    
    
    
    
}
