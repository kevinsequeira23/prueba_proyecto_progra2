/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;


/**
 * Este recibe los datos de un usuario que se quiere registrar
 *
 * @author kevin
 */
public class Registro_Usuarios {

    private int resulatdo_encuesta;
    private String nivel;
    private String leyenda;

    
    public Registro_Usuarios() {
        
    }
    
    /**
     * @return the nivel
     */
    public String getNivel() {
        return nivel;
    }

    /**
     * @param nivel the nivel to set
     */
    public void setNivel(String nivel) {
        if (resulatdo_encuesta <= 19) {
            nivel = "basico";
        } else if (resulatdo_encuesta >= 20 && resulatdo_encuesta <= 40) {
            nivel = "intermedio";
        } else if (resulatdo_encuesta > 40) {
            nivel = "avanzado";
        }
        this.nivel = nivel;
    }

    /**
     * @return the leyenda
     */
    public String getLeyenda() {
        return leyenda;
    }

    /**
     * @param leyenda the leyenda to set
     */
    public void setLeyenda(String leyenda) {
        if (resulatdo_encuesta == 19) {
            leyenda = "“No tienes habilidades de ser un líder;\n"
                    + "carece de dotes de mando. Puede ser por falta de capacidad para\n"
                    + "asumir tal responsabilidad, por tener ideas poco claras, o por su\n"
                    + "forma de ser débil, voluble o maleable. Conviene ayudarle a\n"
                    + "adquirir confianza en sí mismo, a tomar decisiones por sí, a ser\n"
                    + "firme en los propósitos decididos con independencia de opiniones\n"
                    + "ajenas o tendencias sociales.”";
        } else if (resulatdo_encuesta == 20 && resulatdo_encuesta == 40) {
            leyenda = "“No indica liderazgo, pero tampoco\n"
                    + "a una persona pasiva o insegura. Tiene iniciativa propia, pero no\n"
                    + "de arrastrar o influir a los demás de modo suficiente.”";
        } else if (resulatdo_encuesta > 40) {

            leyenda = "“Eres un líder: tiene dotes de mando,\n"
                    + "está seguro de sus propias conclusiones, tiene capacidad de\n"
                    + "iniciativa e influencia en el resto de sus compañeros/as.”";
        }
        this.leyenda = leyenda;
    }

    /**
     * @return the resulatdo_encuesta
     */
    public int getResulatdo_encuesta() {
        return resulatdo_encuesta;
    }

    /**
     * @param resulatdo_encuesta the resulatdo_encuesta to set
     */
    public void setResulatdo_encuesta(int resulatdo_encuesta) {
        this.resulatdo_encuesta = resulatdo_encuesta;
    }
    /**
     * @return the connection
     */
    
}
