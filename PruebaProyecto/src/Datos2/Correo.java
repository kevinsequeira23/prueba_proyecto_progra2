/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;

/**
 * Esta clase permite recibir todos los datos del correo
 *
 * @author kevin
 */
public class Correo {

    private String usuario_correo;
    private String contraseña;
    private String Ruta_archivo;
    private String nombre_archivo;
    private String destino;
    private String asunto;
    private String mensaje;

    public Correo() {
    }

    /**
     * @return the usuario_correo
     */
    public String getUsuario_correo() {
        return usuario_correo;
    }

    /**
     * @param usuario_correo the usuario_correo to set
     */
    public void setUsuario_correo(String usuario_correo) {
        this.usuario_correo = usuario_correo;
    }

    /**
     * @return the contraseña
     */
    public String getContraseña() {
        return contraseña;
    }

    /**
     * @param contraseña the contraseña to set
     */
    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    /**
     * @return the Ruta_archivo
     */
    public String getRuta_archivo() {
        return Ruta_archivo;
    }

    /**
     * @param Ruta_archivo the Ruta_archivo to set
     */
    public void setRuta_archivo(String Ruta_archivo) {
        this.Ruta_archivo = Ruta_archivo;
    }

    /**
     * @return the nombre_archivo
     */
    public String getNombre_archivo() {
        return nombre_archivo;
    }

    /**
     * @param nombre_archivo the nombre_archivo to set
     */
    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the asunto
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * @param asunto the asunto to set
     */
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
