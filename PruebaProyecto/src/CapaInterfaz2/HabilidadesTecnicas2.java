/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaInterfaz2;

import CapaGuardar.Conexion;
import Datos2.Llamar_todas_las_ventana;
import Datos2.Logica_Habilidades_Tecnicas2;
import java.io.IOException;
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author Samir Portillo
 */
public class HabilidadesTecnicas2 extends javax.swing.JDialog {

    /**
     * Creates new form HabilidadesTecnicas2
     */
    Logica_Habilidades_Tecnicas2 ht = new Logica_Habilidades_Tecnicas2();

    public HabilidadesTecnicas2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        colocarTecnicas1();
    }

    public void colocarTecnicas1() {
        LinkedList<String> temp = new LinkedList<>();
        ht.tecnicas1(Conexion.getCedulaStatico());
        temp = ht.retornaDatos();
        for (String datos : temp) {
            System.out.println(datos);
            String[] tempSplit = datos.split(",");
            if (tempSplit[0].equals("basico")) {
                jComboBoxA.setSelectedIndex(0);
            } else if (tempSplit[0].equals("intermedio")) {
                jComboBoxA.setSelectedIndex(1);
            } else if (tempSplit[0].equals("avanzado")) {
                jComboBoxA.setSelectedIndex(2);
            }

            if (tempSplit[1].equals("basico")) {
                jComboBoxB.setSelectedIndex(0);
            } else if (tempSplit[1].equals("intermedio")) {
                jComboBoxB.setSelectedIndex(1);
            } else if (tempSplit[1].equals("avanzado")) {
                jComboBoxB.setSelectedIndex(2);
            }
            if (tempSplit[2].equals("basico")) {
                jComboBoxC.setSelectedIndex(0);
            } else if (tempSplit[2].equals("intermedio")) {
                jComboBoxC.setSelectedIndex(1);
            } else if (tempSplit[2].equals("avanzado")) {
                jComboBoxC.setSelectedIndex(2);
            }

            if (tempSplit[3].equals("basico")) {
                jComboBoxD.setSelectedIndex(0);
            } else if (tempSplit[3].equals("intermedio")) {
                jComboBoxD.setSelectedIndex(1);
            } else if (tempSplit[3].equals("avanzado")) {
                jComboBoxD.setSelectedIndex(2);
            }

            if (tempSplit[4].equals("basico")) {
                jComboBoxE.setSelectedIndex(0);
            } else if (tempSplit[4].equals("intermedio")) {
                jComboBoxE.setSelectedIndex(1);
            } else if (tempSplit[4].equals("avanzado")) {
                jComboBoxE.setSelectedIndex(2);
            }

            if (tempSplit[5].equals("basico")) {
                jComboBoxF.setSelectedIndex(0);
            } else if (tempSplit[5].equals("intermedio")) {
                jComboBoxF.setSelectedIndex(1);
            } else if (tempSplit[5].equals("avanzado")) {
                jComboBoxF.setSelectedIndex(2);
            }

            if (tempSplit[6].equals("basico")) {
                jComboBoxG.setSelectedIndex(0);
            } else if (tempSplit[6].equals("intermedio")) {
                jComboBoxG.setSelectedIndex(1);
            } else if (tempSplit[6].equals("avanzado")) {
                jComboBoxG.setSelectedIndex(2);
            }

            if (tempSplit[7].equals("basico")) {
                jComboBoxH.setSelectedIndex(0);
            } else if (tempSplit[7].equals("intermedio")) {
                jComboBoxH.setSelectedIndex(1);
            } else if (tempSplit[7].equals("avanzado")) {
                jComboBoxH.setSelectedIndex(2);
            }

            if (tempSplit[8].equals("basico")) {
                jComboBoxI.setSelectedIndex(0);
            } else if (tempSplit[8].equals("intermedio")) {
                jComboBoxI.setSelectedIndex(1);
            } else if (tempSplit[8].equals("avanzado")) {
                jComboBoxI.setSelectedIndex(2);
            }

            if (tempSplit[9].equals("basico")) {
                jComboBoxJ.setSelectedIndex(0);
            } else if (tempSplit[9].equals("intermedio")) {
                jComboBoxJ.setSelectedIndex(1);
            } else if (tempSplit[9].equals("avanzado")) {
                jComboBoxJ.setSelectedIndex(2);
            }

            if (tempSplit[10].equals("basico")) {
                jComboBoxK.setSelectedIndex(0);
            } else if (tempSplit[10].equals("intermedio")) {
                jComboBoxK.setSelectedIndex(1);
            } else if (tempSplit[10].equals("avanzado")) {
                jComboBoxK.setSelectedIndex(2);
            }

            if (tempSplit[11].equals("basico")) {
                jComboBoxL.setSelectedIndex(0);
            } else if (tempSplit[11].equals("intermedio")) {
                jComboBoxL.setSelectedIndex(1);
            } else if (tempSplit[11].equals("avanzado")) {
                jComboBoxL.setSelectedIndex(2);
            }

            if (tempSplit[12].equals("basico")) {
                jComboBoxM.setSelectedIndex(0);
            } else if (tempSplit[12].equals("intermedio")) {
                jComboBoxM.setSelectedIndex(1);
            } else if (tempSplit[12].equals("avanzado")) {
                jComboBoxM.setSelectedIndex(2);
            }
            if (tempSplit[13].equals("basico")) {
                jComboBoxN.setSelectedIndex(0);
            } else if (tempSplit[13].equals("intermedio")) {
                jComboBoxN.setSelectedIndex(1);
            } else if (tempSplit[13].equals("avanzado")) {
                jComboBoxN.setSelectedIndex(2);
            }
            if (tempSplit[14].equals("basico")) {
                jComboBoxO.setSelectedIndex(0);
            } else if (tempSplit[14].equals("intermedio")) {
                jComboBoxO.setSelectedIndex(1);
            } else if (tempSplit[14].equals("avanzado")) {
                jComboBoxO.setSelectedIndex(2);
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxB = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxA = new javax.swing.JComboBox<>();
        jComboBoxD = new javax.swing.JComboBox<>();
        jComboBoxC = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jComboBoxF = new javax.swing.JComboBox<>();
        jComboBoxE = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jComboBoxK = new javax.swing.JComboBox<>();
        jComboBoxL = new javax.swing.JComboBox<>();
        jComboBoxN = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jComboBoxJ = new javax.swing.JComboBox<>();
        jComboBoxO = new javax.swing.JComboBox<>();
        jComboBoxM = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jComboBoxG = new javax.swing.JComboBox<>();
        jComboBoxI = new javax.swing.JComboBox<>();
        jComboBoxH = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        btnSiguiente1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 51, 102));
        jLabel1.setText("Habilidades Técnicas 2");

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 51, 102));
        jLabel2.setText("Diseño");

        jComboBoxB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("CSS");

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("HTML");

        jComboBoxA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxC.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("AJAX");

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 51, 102));
        jLabel6.setText("Frameworks");

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Ruby on Rails");

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Node.js");

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("CodeIgniter");

        jComboBoxF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxE.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel11.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 51, 102));
        jLabel11.setText("Herramientas de Desarrollo ");

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("VisualStudio.NET");

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Eclipse");

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("NetBeans");

        jComboBoxK.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxL.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 51, 102));
        jLabel15.setText("Herramientas de Control Versiones ");

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("GIT");

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Tortoise");

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Bitbucket ");

        jComboBoxJ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxO.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxM.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel9.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 51, 102));
        jLabel9.setText("Herramientas de QA ");

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("BadBoy");

        jLabel20.setForeground(new java.awt.Color(0, 0, 0));
        jLabel20.setText("TestComplete");

        jLabel21.setForeground(new java.awt.Color(0, 0, 0));
        jLabel21.setText("Bugzilla ");

        jComboBoxG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxH.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jButton1.setText("modificar");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        btnSiguiente1.setText("Terminar");
        btnSiguiente1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSiguiente1MouseClicked(evt);
            }
        });
        btnSiguiente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguiente1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(61, 61, 61))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel4))
                                    .addGap(54, 54, 54)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jComboBoxC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(39, 39, 39)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel7)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel10))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jComboBoxE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(33, 33, 33)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel21)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                                            .addComponent(jComboBoxI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jComboBoxH, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel19)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jComboBoxG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(21, 21, 21)
                                    .addComponent(jLabel20)))
                            .addComponent(jLabel6)
                            .addComponent(jLabel2))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(80, 80, 80)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel12)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel14))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jComboBoxK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBoxJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBoxL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel15)
                                            .addComponent(jLabel11)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(83, 83, 83)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel16)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jComboBoxM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel17)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jComboBoxN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel18)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                                                .addComponent(jComboBoxO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(61, 61, 61))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSiguiente1)
                                .addGap(23, 23, 23))))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(101, 101, 101)
                        .addComponent(jLabel2)
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jComboBoxA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(jComboBoxJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jComboBoxB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBoxC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14))
                    .addComponent(jComboBoxL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel15))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jComboBoxD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(jComboBoxM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBoxE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel17)
                        .addComponent(jComboBoxN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBoxF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel18)
                        .addComponent(jComboBoxO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jComboBoxG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jComboBoxH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSiguiente1)
                    .addComponent(jButton1))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jComboBoxI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * este metodo valida las habilidades tecnicas del del combox
     */
    public void habilidad_combo() {

        int a = jComboBoxA.getSelectedIndex();
        String contenido;
        String nivel = "";
        if (a == 0) {
            nivel = "basico";
        } else if (a == 1) {
            nivel = "intermedio";
        } else if (a == 2) {
            nivel = "avanzado";
        }
        contenido = nivel;
        ht.setHTML(contenido);

        int b = jComboBoxB.getSelectedIndex();
        String contenido2;
        String nivel2 = "";
        if (b == 0) {
            nivel2 = "basico";
        } else if (b == 1) {
            nivel2 = "intermedio";
        } else if (b == 2) {
            nivel2 = "avanzado";
        }
        contenido2 = nivel2;
        ht.setCSS(contenido2);

        int c = jComboBoxC.getSelectedIndex();
        String contenido3;
        String nivel3 = "";
        if (c == 0) {
            nivel3 = "basico";
        } else if (c == 1) {
            nivel3 = "intermedio";
        } else if (c == 2) {
            nivel3 = "avanzado";
        }
        contenido3 = nivel3;
        ht.setAJAX(contenido);

        int d = jComboBoxD.getSelectedIndex();
        String contenido4;
        String nivel4 = "";
        if (d == 0) {
            nivel4 = "basico";
        } else if (d == 1) {
            nivel4 = "intermedio";
        } else if (d == 2) {
            nivel4 = "avanzado";
        }
        contenido4 = nivel4;
        ht.setRubi_on_rails(contenido4);

        int e = jComboBoxE.getSelectedIndex();
        String contenido5;
        String nivel5 = "";
        if (e == 0) {
            nivel5 = "basico";
        } else if (e == 1) {
            nivel5 = "intermedio";
        } else if (e == 2) {
            nivel5 = "avanzado";
        }
        contenido5 = nivel5;
        ht.setNode(contenido5);

        int f = jComboBoxF.getSelectedIndex();
        String contenido6;
        String nivel6 = "";
        if (e == 0) {
            nivel6 = "basico";
        } else if (e == 1) {
            nivel6 = "intermedio";
        } else if (e == 2) {
            nivel6 = "avanzado";
        }
        contenido6 = nivel6;
        ht.setCodelgniter(contenido6);

        int g = jComboBoxG.getSelectedIndex();
        String contenido7;
        String nivel7 = "";
        if (g == 0) {
            nivel7 = "basico";
        } else if (g == 1) {
            nivel7 = "intermedio";
        } else if (g == 2) {
            nivel7 = "avanzado";
        }
        contenido7 = nivel7;
        ht.setBadboy(contenido7);

        int h = jComboBoxH.getSelectedIndex();
        String contenido8;
        String nivel8 = "";
        if (h == 0) {
            nivel8 = "basico";
        } else if (h == 1) {
            nivel8 = "intermedio";
        } else if (h == 2) {
            nivel8 = "avanzado";
        }
        contenido8 = nivel8;
        ht.setTestcomplete(contenido8);

        int i = jComboBoxI.getSelectedIndex();
        String contenido9;
        String nivel9 = "";
        if (i == 0) {
            nivel9 = "basico";
        } else if (i == 1) {
            nivel9 = "intermedio";
        } else if (i == 2) {
            nivel9 = "avanzado";
        }
        contenido9 = nivel9;
        ht.setBugzilla(contenido9);

        int j = jComboBoxJ.getSelectedIndex();
        String contenido10;
        String nivel10 = "";
        if (j == 0) {
            nivel10 = "basico";
        } else if (j == 1) {
            nivel10 = "intermedio";
        } else if (j == 2) {
            nivel10 = "avanzado";
        }
        contenido10 = nivel10;
        ht.setVisualestudio(contenido10);

        int k = jComboBoxK.getSelectedIndex();
        String contenido11;
        String nivel11 = "";
        if (k == 0) {
            nivel11 = "basico";
        } else if (k == 1) {
            nivel11 = "intermedio";
        } else if (k == 2) {
            nivel11 = "avanzado";
        }
        contenido11 = nivel11;
        ht.setEclipse(contenido11);

        int l = jComboBoxL.getSelectedIndex();
        String contenido12;
        String nivel12 = "";
        if (l == 0) {
            nivel12 = "basico";
        } else if (l == 1) {
            nivel12 = "intermedio";
        } else if (l == 2) {
            nivel12 = "avanzado";
        }
        contenido12 = nivel12;
        ht.setNetbeans(contenido12);

        int m = jComboBoxM.getSelectedIndex();
        String contenido13;
        String nivel13 = "";
        if (m == 0) {
            nivel13 = "basico";
        } else if (m == 1) {
            nivel13 = "intermedio";
        } else if (m == 2) {
            nivel13 = "avanzado";
        }
        contenido13 = nivel13;
        ht.setGit(contenido13);

        int n = jComboBoxN.getSelectedIndex();
        String contenido14;
        String nivel14 = "";
        if (n == 0) {
            nivel14 = "basico";
        } else if (n == 1) {
            nivel14 = "intermedio";
        } else if (n == 2) {
            nivel14 = "avanzado";
        }
        contenido14 = nivel14;
        ht.setTortoise(contenido14);

        int o = jComboBoxO.getSelectedIndex();
        String contenido15;
        String nivel15 = "";
        if (o == 0) {
            nivel15 = "basico";
        } else if (o == 1) {
            nivel15 = "intermedio";
        } else if (o == 2) {
            nivel15 = "avanzado";
        }
        contenido15 = nivel15;
        ht.setBitbucket(contenido15);

    }
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        // TODO add your handling code here:
        ht.modificar(Conexion.getCedulaStatico());
    }//GEN-LAST:event_jButton1MouseClicked

    private void btnSiguiente1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSiguiente1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSiguiente1MouseClicked

    private void btnSiguiente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguiente1ActionPerformed

        int preguntar = Integer.parseInt(JOptionPane.showInputDialog("Volver \n"
                + "1. Log in \n"
                + "2. Sig in"));

        if (preguntar > 2 || preguntar < 1) {
            JOptionPane.showInputDialog("Valores no permitidos");
        } else if (preguntar == 1) {
            dispose();
            Login x = new Login(null, true);
            x.pack();
            x.setVisible(true);
        } else if (preguntar == 2) {
            dispose();
            SigIn x = new SigIn(null, true);
            x.pack();
            x.setVisible(true);
        }


    }//GEN-LAST:event_btnSiguiente1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                HabilidadesTecnicas2 dialog = new HabilidadesTecnicas2(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSiguiente1;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBoxA;
    private javax.swing.JComboBox<String> jComboBoxB;
    private javax.swing.JComboBox<String> jComboBoxC;
    private javax.swing.JComboBox<String> jComboBoxD;
    private javax.swing.JComboBox<String> jComboBoxE;
    private javax.swing.JComboBox<String> jComboBoxF;
    private javax.swing.JComboBox<String> jComboBoxG;
    private javax.swing.JComboBox<String> jComboBoxH;
    private javax.swing.JComboBox<String> jComboBoxI;
    private javax.swing.JComboBox<String> jComboBoxJ;
    private javax.swing.JComboBox<String> jComboBoxK;
    private javax.swing.JComboBox<String> jComboBoxL;
    private javax.swing.JComboBox<String> jComboBoxM;
    private javax.swing.JComboBox<String> jComboBoxN;
    private javax.swing.JComboBox<String> jComboBoxO;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
