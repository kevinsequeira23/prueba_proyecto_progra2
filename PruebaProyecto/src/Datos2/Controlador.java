/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author kevin
 */
public class Controlador {

    /**
     * este metodo es un controlador para poder enviar el correo
     *
     * @param c
     * @return si el correo se puede enviar
     */
    public boolean enviar_correo(Correo c) {
        System.out.println("Hola");

        try {
            Properties p = new Properties();
            p.put("mail.smtp.host", "smtp.gmail.com");
            p.setProperty("mail.smtp.starttls.enable", "true");
            p.setProperty("mail.smtp.port", "587");
            p.setProperty("mail.smtp.user", c.getUsuario_correo());
            p.setProperty("mail.smtp.auth", "true");

            Session s = Session.getDefaultInstance(p);
            BodyPart texto = new MimeBodyPart();
            texto.setText(c.getMensaje());
            BodyPart adjunto = new MimeBodyPart();

            if (!c.getRuta_archivo().equals("")) {
                adjunto.setDataHandler(new DataHandler(new FileDataSource(c.getRuta_archivo())));
                adjunto.setFileName(c.getNombre_archivo());

            }

            MimeMultipart m = new MimeMultipart();
            m.addBodyPart(texto);

            if (!c.getRuta_archivo().equals("")) {
                m.addBodyPart(adjunto);
            }
            MimeMessage mensaje = new MimeMessage(s);
            mensaje.setFrom(new InternetAddress(c.getUsuario_correo()));
            mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getDestino()));
            mensaje.setSubject(c.getAsunto());
            mensaje.setContent(m);

            Transport t = s.getTransport("smtp");
            t.connect(c.getUsuario_correo(), c.getContraseña());
            t.sendMessage(mensaje, mensaje.getAllRecipients());
            t.close();
            return true;

        } catch (MessagingException e) {
            System.out.println("error");
            return false;
        }

    }
}
