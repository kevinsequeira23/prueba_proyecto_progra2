/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaInterfaz2;

import Datos2.Logica_Habilidades_tecnicas;
import java.io.IOException;
import CapaGuardar.*;
import java.util.LinkedList;

/**
 *
 * @author Samir Portillo
 */
public class HabilidadesTecnicas extends javax.swing.JDialog {

    /**
     * Creates new form HabilidadesTecnicas
     */
    Logica_Habilidades_tecnicas habilities = new Logica_Habilidades_tecnicas();
    Conexion co = new Conexion();

    public HabilidadesTecnicas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        habilities.Conexion2();
        setLocationRelativeTo(null);
        colocarTecnicas1();
    }

    public void colocarTecnicas1() {
        LinkedList<String> temp = new LinkedList<>();
        habilities.tecnicas1(Conexion.getCedulaStatico());
        temp = habilities.retornaDatos();
        for (String datos : temp) {
            System.out.println(datos);
            String[] tempSplit = datos.split(",");
            if (tempSplit[0].equals("basico")) {
                jComboBoxA.setSelectedIndex(0);
            } else if (tempSplit[0].equals("intermedio")) {
                jComboBoxA.setSelectedIndex(1);
            } else if (tempSplit[0].equals("avanzado")) {
                jComboBoxA.setSelectedIndex(2);
            }

            if (tempSplit[1].equals("basico")) {
                jComboBoxB.setSelectedIndex(0);
            } else if (tempSplit[1].equals("intermedio")) {
                jComboBoxB.setSelectedIndex(1);
            } else if (tempSplit[1].equals("avanzado")) {
                jComboBoxB.setSelectedIndex(2);
            }
            if (tempSplit[2].equals("basico")) {
                jComboBoxC.setSelectedIndex(0);
            } else if (tempSplit[2].equals("intermedio")) {
                jComboBoxC.setSelectedIndex(1);
            } else if (tempSplit[2].equals("avanzado")) {
                jComboBoxC.setSelectedIndex(2);
            }

            if (tempSplit[3].equals("basico")) {
                jComboBoxD.setSelectedIndex(0);
            } else if (tempSplit[3].equals("intermedio")) {
                jComboBoxD.setSelectedIndex(1);
            } else if (tempSplit[3].equals("avanzado")) {
                jComboBoxD.setSelectedIndex(2);
            }

            if (tempSplit[4].equals("basico")) {
                jComboBoxE.setSelectedIndex(0);
            } else if (tempSplit[4].equals("intermedio")) {
                jComboBoxE.setSelectedIndex(1);
            } else if (tempSplit[4].equals("avanzado")) {
                jComboBoxE.setSelectedIndex(2);
            }

            if (tempSplit[5].equals("basico")) {
                jComboBoxF.setSelectedIndex(0);
            } else if (tempSplit[5].equals("intermedio")) {
                jComboBoxF.setSelectedIndex(1);
            } else if (tempSplit[5].equals("avanzado")) {
                jComboBoxF.setSelectedIndex(2);
            }

            if (tempSplit[6].equals("basico")) {
                jComboBoxG.setSelectedIndex(0);
            } else if (tempSplit[6].equals("intermedio")) {
                jComboBoxG.setSelectedIndex(1);
            } else if (tempSplit[6].equals("avanzado")) {
                jComboBoxG.setSelectedIndex(2);
            }

            if (tempSplit[7].equals("basico")) {
                jComboBoxH.setSelectedIndex(0);
            } else if (tempSplit[7].equals("intermedio")) {
                jComboBoxH.setSelectedIndex(1);
            } else if (tempSplit[7].equals("avanzado")) {
                jComboBoxH.setSelectedIndex(2);
            }

            if (tempSplit[8].equals("basico")) {
                jComboBoxI.setSelectedIndex(0);
            } else if (tempSplit[8].equals("intermedio")) {
                jComboBoxI.setSelectedIndex(1);
            } else if (tempSplit[8].equals("avanzado")) {
                jComboBoxI.setSelectedIndex(2);
            }

            if (tempSplit[9].equals("basico")) {
                jComboBoxJ.setSelectedIndex(0);
            } else if (tempSplit[9].equals("intermedio")) {
                jComboBoxJ.setSelectedIndex(1);
            } else if (tempSplit[9].equals("avanzado")) {
                jComboBoxJ.setSelectedIndex(2);
            }

            if (tempSplit[10].equals("basico")) {
                jComboBoxK.setSelectedIndex(0);
            } else if (tempSplit[10].equals("intermedio")) {
                jComboBoxK.setSelectedIndex(1);
            } else if (tempSplit[10].equals("avanzado")) {
                jComboBoxK.setSelectedIndex(2);
            }

            if (tempSplit[11].equals("basico")) {
                jComboBoxL.setSelectedIndex(0);
            } else if (tempSplit[11].equals("intermedio")) {
                jComboBoxL.setSelectedIndex(1);
            } else if (tempSplit[11].equals("avanzado")) {
                jComboBoxL.setSelectedIndex(2);
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxB = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxA = new javax.swing.JComboBox<>();
        jComboBoxD = new javax.swing.JComboBox<>();
        jComboBoxC = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jComboBoxF = new javax.swing.JComboBox<>();
        jComboBoxE = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jComboBoxH = new javax.swing.JComboBox<>();
        jComboBoxI = new javax.swing.JComboBox<>();
        jComboBoxK = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jComboBoxG = new javax.swing.JComboBox<>();
        jComboBoxL = new javax.swing.JComboBox<>();
        jComboBoxJ = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 51, 102));
        jLabel1.setText("Habilidades Técnicas");

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 51, 102));
        jLabel2.setText("Gestión de proyectos");

        jComboBoxB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Jira");

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Microsoft Proyect");

        jComboBoxA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxD.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxC.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Confluence");

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 51, 102));
        jLabel6.setText("Metodologías de desarrollo");

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Scrum");

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Extreme");

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Programming");

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("kanban");

        jComboBoxF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxE.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel11.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 51, 102));
        jLabel11.setText("Lenguajes de programación");

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("C#");

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Java");

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Python");

        jComboBoxH.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxI.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxK.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jLabel15.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 51, 102));
        jLabel15.setText("Bases de datos");

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("MySQL");

        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("Oracle");

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Mongo DB");

        jComboBoxG.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxL.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jComboBoxJ.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Básico", "Intermedio", "Avanzado" }));

        jButton1.setText("Siguiente");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnModificar.setText("modificar");
        btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(130, 130, 130))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel2)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jComboBoxD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jComboBoxF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel8)
                                                    .addComponent(jLabel9))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jComboBoxE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(71, 71, 71)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(55, 55, 55)
                                        .addComponent(jComboBoxC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel3))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jComboBoxB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBoxA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                        .addComponent(jLabel11))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel12)
                                .addComponent(jLabel14)
                                .addComponent(jLabel13))
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel18)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnModificar)
                                .addGap(23, 23, 23)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jComboBoxH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboBoxI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboBoxG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButton1)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jComboBoxK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(jLabel1)
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel11))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jComboBoxA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jComboBoxG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBoxB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jComboBoxH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel14)
                    .addComponent(jComboBoxI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel15)
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jComboBoxJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16))
                    .addComponent(jComboBoxD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel17)
                                    .addComponent(jComboBoxK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(jLabel18)
                                .addComponent(jComboBoxL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jComboBoxF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(btnModificar))
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * esta metodo valida los combobox de habilidades Tecnicas
     */
    public void HabilidadesT() {

        int a = jComboBoxA.getSelectedIndex();
        String content;
        String habilidad = "";
        String microsoft_proyect = "microsoft_proyect";
        if (a == 0) {
            habilidad = "basico";
        } else if (a == 1) {
            habilidad = "intermedio";
        } else if (a == 2) {
            habilidad = "avanzado";
        }
        content = habilidad;
        habilities.setMicrosoft_proyect(content);

        int b = jComboBoxB.getSelectedIndex();
        String habilidad2 = "";
        String content2;
        String jira = "jira";
        if (b == 0) {
            habilidad2 = "basico";
        } else if (b == 1) {
            habilidad2 = "intermedio";
        } else if (b == 2) {
            habilidad2 = "avanzado";
        }
        content2 = habilidad2;
        habilities.setJira(content2);

        int c = jComboBoxC.getSelectedIndex();
        String habilidad3 = "";
        String content3;
        String confluence = "confluence";
        if (c == 0) {
            habilidad3 = "basico";
        } else if (c == 1) {
            habilidad3 = "intermedio";
        } else if (c == 2) {
            habilidad3 = "avanzado";
        }
        content3 = habilidad3;
        habilities.setConfluence(content3);

        int d = jComboBoxD.getSelectedIndex();
        String habilidad4 = "";
        String content4;
        String scrum = "scrum";
        if (d == 0) {
            habilidad4 = "basico";
        } else if (d == 1) {
            habilidad4 = "intermedio";
        } else if (d == 2) {
            habilidad4 = "avanzado";
        }
        content4 = habilidad4;
        habilities.setScrum(content4);

        int e = jComboBoxE.getSelectedIndex();
        String habilidad5 = "";
        String content5;
        String extreme_programing = "extreme_programing";
        if (e == 0) {
            habilidad5 = "basico";
        } else if (e == 1) {
            habilidad5 = "intermedio";
        } else if (e == 2) {
            habilidad5 = "avanzado";
        }
        content5 = habilidad5;
        habilities.setExtreme_programing(content5);

        int f = jComboBoxF.getSelectedIndex();
        String habilidad6 = "";
        String content6;
        String kanban = "kanban";
        if (f == 0) {
            habilidad6 = "basico";
        } else if (f == 1) {
            habilidad6 = "intermedio";
        } else if (f == 2) {
            habilidad6 = "avanzado";
        }
        content6 = habilidad6;
        habilities.setKanban(content6);

        int g = jComboBoxG.getSelectedIndex();
        String habilidad7 = "";
        String content7;
        String cc = "c#";
        if (g == 0) {
            habilidad7 = "basico";
        } else if (g == 1) {
            habilidad7 = "intermedio";
        } else if (g == 2) {
            habilidad7 = "avanzado";
        }
        content7 = habilidad7;
        habilities.setC(content7);

        int h = jComboBoxH.getSelectedIndex();
        String habilidad8 = "";
        String content8;
        String java = "java";
        if (h == 0) {
            habilidad8 = "basico";
        } else if (h == 1) {
            habilidad8 = "intermedio";
        } else if (h == 2) {
            habilidad8 = "avanzado";
        }
        content8 = habilidad8;
        habilities.setJava(content8);

        int i = jComboBoxI.getSelectedIndex();
        String habilidad9 = "";
        String content9;
        String python = "python";
        if (i == 0) {
            habilidad9 = "basico";
        } else if (i == 1) {
            habilidad9 = "intermedio";
        } else if (i == 2) {
            habilidad9 = "avanzado";
        }
        content9 = habilidad9;
        habilities.setPython(content9);

        int j = jComboBoxJ.getSelectedIndex();
        String habilidad10 = "";
        String content10;
        String mySQL = "mySQL";
        if (j == 0) {
            habilidad10 = "basico";
        } else if (j == 1) {
            habilidad10 = "intermedio";
        } else if (j == 2) {
            habilidad10 = "avanzado";
        }
        content10 = habilidad10;
        habilities.setMySQL(content10);

        int k = jComboBoxK.getSelectedIndex();
        String habilidad11 = "";
        String content11;
        String oracle = "oracle";
        if (k == 0) {
            habilidad11 = "basico";
        } else if (k == 1) {
            habilidad11 = "intermedio";
        } else if (k == 2) {
            habilidad11 = "avanzado";
        }
        content11 = habilidad11;
        habilities.setOracle(content11);

        int l = jComboBoxL.getSelectedIndex();
        String habilidad12 = "";
        String content12;
        String mongoBD = "mongoBD";
        if (l == 0) {
            habilidad12 = "basico";
        } else if (l == 1) {
            habilidad12 = "intermedio";
        } else if (l == 2) {
            habilidad12 = "avanzado";
        }
        content12 = habilidad12;
        habilities.setMongoBD(content12);

    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        // TODO add your handling code here:
        HabilidadesT();
        try {
            habilities.insertar();
        } catch (IOException ex) {
            System.out.println("no se pudo");
        }
        HabilidadesTecnicas2 habilidades2 = new HabilidadesTecnicas2(null, true);
        habilidades2.pack();
        habilidades2.setVisible(true);
    }//GEN-LAST:event_jButton1MouseClicked

    private void btnModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarMouseClicked
        // TODO add your handling code here:
        HabilidadesT();
        habilities.modificar(Conexion.getCedulaStatico());
    }//GEN-LAST:event_btnModificarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HabilidadesTecnicas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                HabilidadesTecnicas dialog = new HabilidadesTecnicas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JComboBox<String> jComboBoxA;
    private javax.swing.JComboBox<String> jComboBoxB;
    private javax.swing.JComboBox<String> jComboBoxC;
    private javax.swing.JComboBox<String> jComboBoxD;
    private javax.swing.JComboBox<String> jComboBoxE;
    private javax.swing.JComboBox<String> jComboBoxF;
    private javax.swing.JComboBox<String> jComboBoxG;
    private javax.swing.JComboBox<String> jComboBoxH;
    private javax.swing.JComboBox<String> jComboBoxI;
    private javax.swing.JComboBox<String> jComboBoxJ;
    private javax.swing.JComboBox<String> jComboBoxK;
    private javax.swing.JComboBox<String> jComboBoxL;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
