/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos2;

import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Samir Portillo
 */
public class Graficas_Mongo {
     public void graficarPastelMongo(int basico, int intermedion, int avanzado){

        JFreeChart grafico = null;
        DefaultCategoryDataset datos = new DefaultCategoryDataset();
        datos.addValue(basico,"Grafica","Básico");
        datos.addValue(intermedion,"Grafica","intermedio");
        datos.addValue(avanzado,"Grafica","Avanzado");
        
        DefaultPieDataset pastel = new DefaultPieDataset();
        pastel.setValue("Básico", basico);
        pastel.setValue("Intermedio", intermedion);
        pastel.setValue("Avanzado", avanzado);
        
        grafico = ChartFactory.createPieChart("Niveles en Mongo",pastel,true, true, false);
        
        
       ChartPanel cPanel = new ChartPanel(grafico);
        
       JFrame info = new JFrame();
      
       info.getContentPane().add(cPanel);
        
       info.pack();
       info.setVisible(true);
       info.setLocationRelativeTo(null);
   
    }
}
